package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements  CellAutomaton {
    IGrid currentGeneration;
    GameOfLife n;

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }


    public BriansBrain(int row, int col){
        currentGeneration = new CellGrid(row, col, CellState.DEAD);
        initializeCells();
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        // TODO

        for (int row = 0; row < numberOfRows();row++) {
            for (int col = 0; col < numberOfColumns();col++) {
                nextGeneration.set(row, col, getNextCell(row, col));
            }
        }
        currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {

        int numNeighbours = countNeighbors(row, col, CellState.ALIVE); // antall naboer
        CellState state = currentGeneration.get(row, col);
        if (state == CellState.ALIVE) {
            return CellState.DYING;
        }
        if (state == CellState.DYING) {
            return CellState.DEAD;

        }if (state == CellState.DEAD && numNeighbours == 2) {
            return CellState.ALIVE;

        }if (state == CellState.DEAD) {
                return CellState.DEAD;

        }
        return state;

    }

    private int countNeighbors(int row, int col, CellState alive) {
        int neighborCount = 0;

        for (int r = row-1; r <= row + 1 ; r++) {
            for (int c = col-1; c <= col + 1; c++) {
                if((!(r == row && c == col)
                        && (r >= 0 && r < currentGeneration.numRows()) // sjekker indexoutofbound
                        && (c >= 0 && c < currentGeneration.numColumns())
                        && (currentGeneration.get(r, c)) == alive)){
                    neighborCount++;
                }
            }
        }
        return neighborCount;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}

